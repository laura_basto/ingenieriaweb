<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\SessionController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\ProveedorController;

Route::get('/', function () {
    return view('home');
})
->middleware('auth');

Route::get('/register', [RegisterController::class, 'create'])
    ->middleware('guest')
    ->name('register.index');
Route::post('/register', [RegisterController::class, 'store'])
    ->name('register.store');

Route::get('/login', [SessionController::class, 'create'])
    ->middleware('guest')
    ->name('login.index');
Route::post('/login', [SessionController::class, 'store'])
    ->name('login.store');

Route::get('/logout', [SessionController::class, 'destroy'])
    ->middleware('auth')
    ->name('login.destroy');

Route::get('/admin', [AdminController::class, 'index'])
    ->middleware('auth.admin')
    ->name('admin.index');

//Rutas de productos
Route::get('/products', [ProductsController::class, 'modul'])
    ->name('products.modul');

Route::get('/products/create', [ProductsController::class, 'create'])
    ->name('products.create');

Route::post('/products/create', [ProductsController::class, 'store'])
    ->name('products.store');

/*Route::get('/products/destroy', [ProductsController::class, 'destroy'])
    ->name('products.destroy');*/

  /*  Route::delete('products/{id}', function ($id) {
        echo "pruebaaaa";
    });*/

//Rutas de proveedores
Route::get('/proveedores', [ProveedorController::class, 'modul']);


