<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->integer('codigo_producto');
            $table->string('nombre_producto');
            $table->string('descripcion_producto');
            $table->integer('id_proveedor');
            $table->decimal('precio_entrada', 10, 2);
            $table->decimal('cantidad_producto', 10, 2);
            $table->decimal('precio_salida', 10, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
