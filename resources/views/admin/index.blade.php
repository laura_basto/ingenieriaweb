@extends('layouts.app')

@section('title', 'Admin')

@section('content')

<body class="bg-gray-200">
<!--Modulos-->   
    <section class="mt-20 px-80" >      
        <div class="border-transparent rounded-md shadow-2xl pt-10 py-12">
            <h1 class="text-5xl text-center pt-2 font-semibold" >Módulos</h1>
        <!--Usuarios-->
            <div class="inline-block box-content border-2 border-transparent shadow-2xl 
                rounded-md mt-14 ml-20 mr-16 py-5 px-9 hover:bg-red-700 hover:text-white">                
                <img class="w-10 md:w-24 lg:w-34" src="..\resources\assets\usuarios.png" alt="Usuarios" href="" >
                <h4 class="text-lg text-center pt-2 font-semibold">Usuarios</h4>
            </div>
        <!--Productos-->
            <div class="inline-block box-content border-2 border-transparent shadow-2xl 
                rounded-md mt-14 ml-16 mr-16 py-5 px-9 hover:bg-red-700 hover:text-white">
               <a href="{{route('products.modul')}}">
                <img class="w-10 md:w-24 lg:w-34" src="..\resources\assets\productos.png" alt="Productos">
                <h4 class="text-lg text-center pt-2 font-semibold">Productos</h4></a>
            </div> 
        <!--Proveedores-->
            <div class="inline-block box-content border-2 border-transparent shadow-2xl 
                rounded-md mt-14 ml-16 mr-16 py-5 px-9 hover:bg-red-700 hover:text-white">
                <img class="w-10 md:w-24 lg:w-34" src="..\resources\assets\proveedor.png" alt="Proveedores">
                <h4 class="text-lg text-center pt-2 font-semibold">Proveedores</h4>
            </div>
        <!--Clientes-->
            <div class="inline-block box-content border-2 border-transparent shadow-2xl 
                rounded-md mt-14 ml-16 mr-16 py-5 px-9 hover:bg-red-700 hover:text-white">
                <img class="w-10 md:w-24 lg:w-34" src="..\resources\assets\cliente.png" alt="Clientes">
                <h4 class="text-lg text-center pt-2 font-semibold">Clientes</h4>
            </div>
        <!--Facturas-->
            <div class="inline-block box-content border-2 border-transparent shadow-2xl 
                rounded-md mt-14 ml-20 mr-16 py-5 px-9 hover:bg-red-700 hover:text-white">
                <img class="w-10 md:w-24 lg:w-34" src="..\resources\assets\factura.png" alt="Facturas"
                href="" >
                <h4 class="text-lg text-center pt-2 font-semibold">Facturas</h4>
            </div> 
        <!--Inventario-->
            <div class="inline-block box-content border-2 border-transparent shadow-2xl 
                rounded-md mt-14 ml-16 mr-16 py-5 px-9 hover:bg-red-700 hover:text-white">
                <img class="w-10 md:w-24 lg:w-34" src="..\resources\assets\inventario.png" alt="Inventario">
                <h4 class="text-lg text-center pt-2 font-semibold">Inventario</h4>
            </div>
        <!--Configuracion-->
            <div class="inline-block box-content border-2 border-transparent shadow-2xl 
                rounded-md mt-14 ml-16 mr-16 py-5 px-9 hover:bg-red-700 hover:text-white">
                <img class="w-10 md:w-24 lg:w-34" src="..\resources\assets\configuracion.png" alt="Configuracion">
                <h4 class="text-lg text-center pt-2 font-semibold">Mi cuenta</h4>
            </div>
        <!--Acerca de-->
            <div class="inline-block box-content border-2 border-transparent shadow-2xl 
                rounded-md mt-14 ml-16 mr-16 py-5 px-9 hover:bg-red-700 hover:text-white">
                <img class="w-10 md:w-24 lg:w-34" src="..\resources\assets\acerca-de.png" alt="Acerca-de">
                <h4 class="text-lg text-center pt-2 font-semibold">Acerca de</h4>
            </div>
        </div>
    </section>
@yield('content')
</body>
@endsection