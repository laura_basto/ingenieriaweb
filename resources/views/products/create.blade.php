@extends('layouts.app')

@section('title', 'Crear producto')

@section('content')

<nav class="h-16 flex justify-end py-10 px-60">
    <a href=" {{route('products.modul')}}" class="text-center text-xl text-red-600 rounded-lg px-2 h-10 bg-white
    font-semibold hover:bg-red-600 hover:text-white shadow-2xl font-bold border-2 border-gray-300">Módulo de productos</a>
</nav>

<div class="ml-10 mr-80 flex justify-center pt-20" >
    <form action="{{route('products.store')}}" method="POST" class="bg-gray-100 w-1/4 p-7 border-gray-100 shadow-2xl ml-80 
        rounded-lg">    
    @csrf
        <h2 class="text-2xl text-center py-4 mb-4 font-semibold">Crear producto</h2>

        <input class="my-2 w-full border-2 border-gray-300 px-3 bg-white-200 text-lg rounded-md placeholder-gray-500 focus:bg-white"
            placeholder="Código" name="codigo_producto">

        <input class="my-2 w-full border-2 border-gray-300 px-3 bg-white-200 text-lg rounded-md placeholder-gray-500 focus:bg-white"
            placeholder="Nombre" name="nombre_producto">

        <input class="my-2 w-full border-2 border-gray-300 px-3 bg-white-200 text-lg rounded-md placeholder-gray-500 focus:bg-white"
            placeholder="Descripción" name="descripcion_producto">

        <input class="my-2 w-full border-2 border-gray-300 px-3 bg-white-200 text-lg rounded-md placeholder-gray-500 focus:bg-white"
            placeholder="Id proveedor" name="id_proveedor">

        <input class="my-2 w-full border-2 border-gray-300 px-3 bg-white-200 text-lg rounded-md placeholder-gray-500 focus:bg-white"
            placeholder="Costo" name="precio_entrada">

        <input class="my-2 w-full border-2 border-gray-300 px-3 bg-white-200 text-lg rounded-md placeholder-gray-500 focus:bg-white"
            placeholder="Cantidad" name="cantidad_producto">

        <input class="my-2 w-full border-2 border-gray-300 px-3 bg-white-200 text-lg rounded-md placeholder-gray-500 focus:bg-white"
            placeholder="Precio de venta" name="precio_salida">

        <button type="submit" class="my-3 w-full bg-red-600 p-2 font-bold text-xl
        text-white hover:bg-gray-300 hover:text-black border-gray-300 border-2 rounded-lg">Crear</button>
    </form>
</div>


@endsection