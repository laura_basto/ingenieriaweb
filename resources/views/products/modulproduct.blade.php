@extends('layouts.app')

@section('title', 'Modulo productos')

@section('content')


<script src="https://kit.fontawesome.com/a23e6feb03.js"></script>

<link rel="stylesheet" 
    href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" 
    href="https://cdnjs.cloudflare.com/ajax/libs/tailwindcss/2.0.1/tailwind.min.css">
<!--Boostrap -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
  
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src = "https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>    

<script>
    $(document).ready(function(){
        $('#example').DataTable();
    });
</script>
<!--cierre Boostrap -->
<h1 class="text-5xl text-center mt-12 font-semibold">Productos</h1>
<!--Tabla de producto-->
<div class="max-w-8xl mx-40 sm:px-6 lg:px-8 mt-20 " style="width:80%">
    <div class="bg-white overflow-hidden shadow-2xl sm:rounded-lg">
        <table id="example" class="table-auto w-60 pt-4 p" >
            <thead>
                <tr class="bg-red-700 text-white pt-20">
                    <th class="w-2 py-4 text-center">Id</th>
                    <th class="w-6 py-4 text-center">Codigo</th>
                    <th class="w-8 py-4 text-center">Producto</th>
                    <th class="w-40 py-4 text-center">Descripción</th>    
                    <th class="w-10 py-4 text-center">Id proveedor</th>  
                    <th class="w-10 py-4 text-center">Costo por unidad</th> 
                    <th class="w-8 py-4 text-center">Cantidad</th>
                    <th class="w-10 py-4 text-center">Precio de venta</th>
                    <th class="w-8 py-4  text-center">Acciones</th>

                </tr>
            </thead>
            <tbody>
                @foreach ($products as $row)
                    <tr>
                        <td class="py-3 px-4 text-center">{{$row->id}}</td>
                        <td class="py-3 px-6 text-center">{{$row->codigo_producto}}</td>
                        <td class="p-3 text-center">{{$row->nombre_producto}}</td>
                        <td class="p-3 text-center">{{$row->descripcion_producto}}</td>
                        <td class="p-3 text-center">{{$row->id_proveedor}}</td>
                        <td class="p-3 text-center">{{$row->precio_entrada}}</td>
                        <td class="p-3 text-center">{{$row->cantidad_producto}}</td>
                        <td class="p-3 text-center">{{$row->precio_salida}}</td>
                        <td class="p-3 text-center flex justify-center">

                            <form action=" " method="POST" >
                                @csrf
                                @method('delete')
                                <button class="bg-red-500 text-white px-3 py-1 rounded-sm mx-2">
                                    <i class="fas fa-trash"></i></button> <!--Boton Borrar-->
                            </form>
                            <!--Boton Editar-->
                            <a href=" {{url('products.edit', $row->id)}} " class="bg-green-500 
                                text-white px-3 py-1 rounded-sm"><i class="fas fa-pen"></i></a>
                            
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div> 

<nav class="flex justify-center py-14 mr-60">
    <a href="{{route('products.create')}}" class="flex justify-center text-xl text-red-600 rounded-lg px-2 h-10 bg-white
        font-semibold hover:bg-red-600 hover:text-white shadow-2xl font-bold border-2 border-red-600">Crear producto</a>
    
    <a href="{{route('admin.index')}}" class="ml-8 flex justify-center text-xl text-red-600 rounded-lg px-2 h-10 bg-white
        font-semibold hover:bg-red-600 hover:text-white shadow-2xl font-bold border-2 border-red-600 ">Home</a>
</nav>
@endsection