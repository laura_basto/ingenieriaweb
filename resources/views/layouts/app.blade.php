<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>@yield('title') - SisGI 2 App</title>
  
    <script src="https://kit.fontawesome.com/a23e6feb03.js"></script>
    <link rel="stylesheet" 
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" 
        href="https://cdnjs.cloudflare.com/ajax/libs/tailwindcss/2.0.1/tailwind.min.css">
  </head>
  <body class="bg-gray-100 text-gray-800">
    <nav class="flex py-5 bg-red-700 text-white">
        <div class="w-1/2 px-12 mr-auto">
            <p class="text-5xl font-bold">SiSGi 2</p>
        </div>

        <ul class="w-1/2 px-16 ml-auto flex justify-end pt-1">
            @if (auth()->check())
                <li class="mx-8">  
                    <p class="text-xl"> <b>{{auth()->user()->name}}</b></p>
                </li>
                <li>
                    <a href="{{route('login.destroy')}}" class="font-bold
                    border-2 border-white-500 py-3 px-2 rounded-md hover:bg-white
                    hover:text-red-700">Cerrar sesión</a>    
                </li>
            @else
                <li class="mx-6">  
                    <a href="{{route('login.index')}}" class="font-semibold 
                    hover:bg-red-600 py-5 px-4 rounded-md">Ingreso</a>
                </li>
                <li>
                    <a href="{{route('register.index')}}" class="font-semibold 
                    border-2 border-white-500 py-4 px-4 rounded-md hover:bg-white
                    hover:text-red-700">Registro</a>    
                </li>  
            @endif 
        </ul>
    </nav>

    @yield('content')
  </body>
</html>