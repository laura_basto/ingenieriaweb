<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'codigo_producto',
        'nombre_producto',
        'id_proveedor',
        'precio_entrada',
        'cantidad_producto',
        'precio_salida'
    ];
}
