<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    use HasFactory;


    protected $fillable = [
        'id_proveedor',
        'nombre_proveedor',
        'marca_proveedor',
        'nit_proveedor',
        'celular_proveedor',
        'direccion_proveedor',
        'correo_proveedor'
    ];
}
