<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ProductsController extends Controller
{
    public function modul(){

        $products = Product::all();
        
        return view ('products.modulproduct', compact('products'));
    }


    public function create(){
        return view('products.create');
    }

    public function store(Request $request){
        $product = new Product();

        $product->codigo_producto = $request->codigo_producto;
        $product->nombre_producto = $request->nombre_producto;
        $product->descripcion_producto = $request->descripcion_producto;
        $product->id_proveedor = $request->id_proveedor;
        $product->precio_entrada = $request->precio_entrada;
        $product->cantidad_producto = $request->cantidad_producto;
        $product->precio_salida = $request->precio_salida;

        $product->save();

        return redirect()->route('products.modul');
    }


    public function edit($id){

        $product = Product::find($id);
        
        return view('products.edit', compact('product'));
    }

    public function destroy($id){
        
        $product = Product::find($id);

        $product->delete();

        return redirect()->route('products.modul');
    }
}
