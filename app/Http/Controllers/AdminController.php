<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index(){
        return view('admin.index');
    }

    public function store(){
        
        if(auth()->user()->role == 'admin'){
            return redirect()->route('admin.index');
        }else{
            return redirect()->to('/');
        }        
    }
}
